# Output load balancer dns name
output "dns_name" {
  description = "The DNS name of the load balancer."
  value       = aws_lb.webapp_lb.dns_name
}