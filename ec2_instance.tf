/* 
 * define web servers
 * create security group for ec2 instances (inbound and outbound)
 * create security group for ec2-mysql instances (inbound and outbound)
 * create iam role for allowing ec2 instances to access s3 bucket
 * create ec2 instances (webapp{01, 02}) 
*/

locals {
  web_servers = {
    web01 = {
      machine_type = var.instance_type
      subnet_id    = aws_subnet.private_us_east_1a.id
    }
    web02 = {
      machine_type = var.instance_type
      subnet_id    = aws_subnet.private_us_east_1b.id
    }
  }
}

resource "aws_security_group" "ec2_sg" {
  name   = "ec2_sg"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "ec2_sg"
  }
}

resource "aws_security_group_rule" "web_ec2_ingress" {
  type                     = "ingress"
  from_port                = var.lb_port
  to_port                  = var.lb_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ec2_sg.id
  source_security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "web_ec2_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ec2_sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ec2_sql_ingress" {
  type                     = "ingress"
  from_port                = var.db_port
  to_port                  = var.db_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ec2_sg.id
  source_security_group_id = aws_security_group.sql_sg.id
}


resource "aws_iam_role" "SSMRoleForEC2" {
  name = "SSMRoleForEC2"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
{
"Effect": "Allow",
"Principal": {
"Service": "ec2.amazonaws.com"
},
"Action": "sts:AssumeRole"
}
]
}
EOF

  tags = {
    Name = "SSMRoleForEC2"
  }
}

resource "aws_iam_instance_profile" "web_ec2_iam_profile" {
  name = "SSMRoleForEC2"
  role = aws_iam_role.SSMRoleForEC2.name

  tags = {
    Name = "web_ec2_iam_profile"
  }
}

resource "aws_iam_role_policy_attachment" "s3-role-policy-attachment" {
  for_each = toset([
    "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore",
    "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
  ])
  role       = aws_iam_role.SSMRoleForEC2.name
  policy_arn = each.value
}

resource "aws_instance" "webapp" {
  for_each = local.web_servers

  ami                    = var.ami
  instance_type          = each.value.machine_type
  iam_instance_profile   = aws_iam_instance_profile.web_ec2_iam_profile.name
  vpc_security_group_ids = [aws_security_group.ec2_sg.id]
  subnet_id              = each.value.subnet_id
  user_data              = <<EOF
    #! /bin/bash
    sudo yum update -y
    sudo yum install -y httpd
    echo "<h1>Welcome !!</h1>" | sudo tee /var/www/html/index.html
    sudo systemctl start httpd
    sudo systemctl enable httpd
    EOF

  root_block_device {
    volume_size = var.disk_size
  }

  tags = {
    Name = each.key
  }
}