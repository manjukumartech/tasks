# tasks

## Terraform code to provision infrastructure in AWS.
#
```
├── README.md
├── ec2_instance.tf
├── eks-node.tf
├── eks.tf
├── internet_gateway.tf
├── lb.tf
├── load_balancer.tf
├── nat.tf
├── outputs.tf
├── providers.tf
├── route_table.tf
├── s3.tf
├── sql.tf
├── subnets.tf
├── variables.tf
└── vpc.tf
```
* ec2_instance - create ec2 instances along with necessary security groups and iam roles for allowing access to mysql and s3
* eks-node - Create worker nodes for eks
* eks - create kubernetes cluster
* internet_gateway - create a internet gateway
* load_balancer - create and configure application load balancer
* nat - create nat gateway
* outputs - display the load balancer dns name
* route_table - create route table and associate with the subnets
* s3 - create bucket and iam role for allowing access from ec2 instances
* sql - create mysql and security group to allow access from ec2 instances
* subnets - create subnets for public and private access
* variables - vars file
* vpc - create a new vpc