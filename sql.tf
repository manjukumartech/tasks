/* 
  * create subnet group for mysql 
  * create security group for sql instance to allow access to ec2
  * create mysql instance
*/
resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "db_subnet_group"
  subnet_ids = [aws_subnet.private_us_east_1a.id, aws_subnet.private_us_east_1b.id]

  tags = {
    Name = "db_subnet_group"
  }
}

resource "aws_security_group" "sql_sg" {
  name   = "sql_sg"
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "sql_sg"
  }
}

resource "aws_security_group_rule" "sql_ec2_ingress" {
  type                     = "ingress"
  from_port                = var.db_port
  to_port                  = var.db_port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.sql_sg.id
  source_security_group_id = aws_security_group.ec2_sg.id
}

resource "aws_security_group_rule" "sql_ec2_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.sql_sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_db_instance" "web_mysql" {
  allocated_storage      = 10
  engine                 = "mysql"
  instance_class         = "db.t3.micro"
  db_name                = "web_mysql"
  username               = "web_user"
  password               = "web_user"
  skip_final_snapshot    = true
  availability_zone      = var.db_zone
  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.id
  vpc_security_group_ids = [aws_security_group.sql_sg.id]

  tags = {
    Name = "web_mysql"
  }
}

