variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "lb_port" {
  type    = number
  default = 80
}

variable "db_port" {
  type    = number
  default = 3306
}

variable "ami" {
  type    = string
  default = "ami-09d3b3274b6c5d4aa"
}

variable "disk_size" {
  type    = number
  default = 20
}

variable "eks_node_type" {
  type    = string
  default = "t3.large"
}

variable "db_zone" {
  type    = string
  default = "us-east-1a"
}



